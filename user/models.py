from django.db import models

class Katalog(models.Model):
    gambar_produk = models.ImageField()
    nama = models.AutoField(primary_key=True)
    alamat = models.CharField(max_length=200)
    harga = models.CharField(max_length=10)
    deskripsi = models.CharField(max_length=100)