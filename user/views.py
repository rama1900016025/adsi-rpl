from multiprocessing import context
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from .forms import RegisterUserForm

# Create your views here.
def signin(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        print(username)
        if user is not None:
           login(request, user)
           return redirect('home')
        else:
            messages.success(request, ('Error Login, Try Again...'))
    else:
        context = {
            # 'user' : superuser
        }
    return render(request, 'user/login.html')

def signup(request):
    if request.method == "POST":
        form = RegisterUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, ('Register Success'))
            return redirect('signin')
        else:
            messages.success(request, ('Username Alredy'))
    else:
        form = RegisterUserForm()
    return render(request, 'user/register.html', {'form':form})

def signout(request):
    logout(request)
    return redirect ('signin')